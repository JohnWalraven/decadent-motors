<?php
session_start();
if (!isset($_SESSION["emailAddress"]) && !isset($_SESSION["password"])) {
    $loginStatus = "Currently not logged in";
    $accessToSell = false;
    $accessToAccount = false;
} else {
    $loginStatus = "Logged in as " . (isset($_SESSION['name']) ? $_SESSION['name'] : '') . "<input type='submit' value='Sign out'/>";
    $accessToSell = true;
    $accessToAccount = true;
}

$serverName = "localhost:3306";
$dbUsername = "root";
$dbPassword = "";
$dbName = "decadentmotortrade";

$connection = mysqli_connect($serverName, $dbUsername, $dbPassword, $dbName);

if ($connection->connect_error) {
    die("Connection failed: " . $connection->connect_error);
}

$resultCarsBeingSold = mysqli_query($connection, "SELECT Car_Make, Car_Model, Car_Price, Car_Location FROM cars ORDER BY Car_Price ASC;");

while ($rowCar = mysqli_fetch_array($resultCarsBeingSold, MYSQL_ASSOC)) {
    $Car_Make[] = "<option>" . $rowCar["Car_Make"] . "</option>";
    $Car_Model[] = "<option>" . $rowCar["Car_Model"] . "</option>";
    $Car_location[] = "<option>" . $rowCar["Car_Location"] . "</option>";
}
$connection->close();

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Decadent Motor Trade</title>
    <link rel="stylesheet" type="text/css" href="../CSS/Theme.css">
    <script src="../Javascript/JavaScriptFile.js"></script>
</head>

<header>
    <div id="signOut">
        <form method="post" action="Logout.php">
            <label><?php echo $loginStatus; ?></label>
        </form>
    </div>
    <div id="signIn">
        <form method="post" action="login.php">
            <input type="text" id="loginEmailAddress" name="inputLoginEmailAddress"
                   placeholder="Email Address or Username"
                   onblur="testValidity('loginEmailAddress','requiredLoginEmailAddress' )">
            <input type="password" id="loginPassword" name="inputLoginPassword" placeholder="Password"
                   onblur="testValidity('loginPassword','requiredLoginPassword' )">
            <input type="submit" name="submitDetails" value="Sign In" onclick="validateLogin()"><br>
            <label class="requiredField" id="requiredLoginEmailAddress">This field is required</label>
            <label class="requiredField" id="requiredLoginPassword" style="padding-left:6%; ">This field is
                required</label>
        </form>
    </div>
    <div id="header">
        <div id="logo-div">
            <a href="Home Page.php"><img class="logo-picture" src="../Images/Drawing.png"/></a>
        </div>
        <div id="navigation-bar">
            <div class="navigation-button"><a href="Buy.php">Buy</a></div>
            <div class="navigation-button"><a href="<?php if ($accessToSell == true) {
                    echo 'Sell.php';
                } ?>">Sell</a></div>
            <div class="navigation-button"><a href="<?php if ($accessToSell == true) {
                    echo 'My%20Account.php';
                } ?>">My Account</a></div>
            <div class="navigation-button"><a href="Feedback.php">Feedback</a></div>
            <div class="navigation-button"><a href="About%20US.php">About Us</a></div>
        </div>
    </div>
</header>
<body>
<div id="advancedSearch">
    <span class="FindYourCar">Find Your Car Your Way</span><br><br>
    <form method="post" action="searchResults.php">
        <div class="advancedSearchingToggles">
            <div class="priceBoxes">
                <label class="divisionLabel">Location</label><br><br>
                <select class="advancedSearchSelectBox" name="searchCarLocation">
                    <option>Any Location</option>
                    <?php foreach ($Car_location as $value) {
                        echo $value;
                    } ?>
                </select>
            </div>
            <br><br><br>
            <div class="priceBoxes">
                <label class="divisionLabel"> Price</label><br><br>
                <span>Minimum</span><br>
                <select class="advancedSearchSelectBox" name="searchMinimumPrice">
                    <option>Any Price</option>
                    <option>$10000</option>
                    <option>$20000</option>
                    <option>$40000</option>
                    <option>$60000</option>
                    <option>$70000</option>
                    <option>$90000</option>
                    <option>$120,000</option>
                    <option>$150,000</option>
                    <option>$170,000</option>
                    <option>$200,000</option>
                </select>
            </div>
            <div class="priceBoxes">
                <span>Maximum</span><br>
                <select class="advancedSearchSelectBox" name="searchMaximumPrice">
                    <option>Any Price</option>
                    <option>$400000</option>
                    <option>$600000</option>
                    <option>$700000</option>
                    <option>$900000</option>
                    <option>$1,200,000</option>
                    <option>$1,500,000</option>
                    <option>$1,700,000</option>
                    <option>$2,000,000</option>
                </select>
            </div>
            <div class="yearOfCars"><br><br>
                <label class="divisionLabel"> Year</label><br><br>
                <span>From</span><br>
                <select class="advancedSearchSelectBox" name="searchFromYear">
                    <option>Any Year</option>
                    <option>2000</option>
                    <option>2001</option>
                    <option>2002</option>
                    <option>2003</option>
                    <option>2004</option>
                    <option>2005</option>
                    <option>2006</option>
                    <option>2007</option>
                    <option>2008</option>
                    <option>2009</option>
                    <option>2010</option>
                    <option>2011</option>
                    <option>2012</option>
                    <option>2013</option>
                    <option>2014</option>
                    <option>2015</option>
                    <option>2016</option>
                </select>
            </div>
            <div class="yearOfCars"><br><br>
                <span>To</span><br>
                <select class="advancedSearchSelectBox" name="searchToYear">
                    <option>Any Year</option>
                    <option>2000</option>
                    <option>2001</option>
                    <option>2002</option>
                    <option>2003</option>
                    <option>2004</option>
                    <option>2005</option>
                    <option>2006</option>
                    <option>2007</option>
                    <option>2008</option>
                    <option>2009</option>
                    <option>2010</option>
                    <option>2011</option>
                    <option>2012</option>
                    <option>2013</option>
                    <option>2014</option>
                    <option>2015</option>
                    <option>2016</option>
                </select>
            </div>
        </div>
        <div class="makeOfCars"><br><br>
            <label class="divisionLabel"> Make</label><br><br>
            <select class="advancedSearchSelectBox" name="searchCarMake">
                <option>Any Make</option>
                <?php foreach ($Car_Make as $value) {
                    echo $value;
                } ?>
            </select>
        </div>

        <div class="makeOfCars"><br><br>
            <label class="divisionLabel"> Model</label><br><br>
            <select class="advancedSearchSelectBox" name="searchCarModel">
                <option>Any Model</option>
                <?php foreach ($Car_Model as $value) {
                    echo $value;
                } ?>
            </select>
        </div>
        <div id="searchButton">
            <br>
            <input class="submitButton" type="submit" name="search" value="Search"/>
        </div>
    </form>
</div>
<div class="table">
    <span class="featuredCarsSpan">Featured Listings</span>
    <div class="table-row ">
        <div class="table-cell">
            <div class="flipcard v">
                <div class="front">
                    <img class="featuredCars" src="../Cars%20For%20Sale/ferrari_enzo_red.jpg"/>
                </div>
                <div class="back">
                    <p class="carInfo">2015 Ferrari Enzo<br>500km on clock<br>$2.9 Million</p>
                </div>
            </div>
        </div>
        <div class="table-cell">
            <div class="flipcard v">
                <div class="front">
                    <img class="featuredCars" src="../Cars%20For%20Sale/alfa-romeo-4c.jpg"/>
                </div>
                <div class="back">
                    <p class="carInfo">2015 Alfa Romeo 4C<br>1000km on clock<br>$54,900</p>
                </div>
            </div>
        </div>
        <div class="table-cell">
            <div class="flipcard v">
                <div class="front">
                    <img class="featuredCars" src="../Cars%20For%20Sale/maybach_62.jpg"/>
                </div>
                <div class="back">
                    <p class="carInfo">2008 Maybach 62<br>9000km on clock<br>$194,000</p>
                </div>
            </div>
        </div>
    </div>
    <div class="table-row">
        <div class="table-cell">
            <div class="flipcard v">
                <div class="front">
                    <img class="featuredCars" src="../Cars%20For%20Sale/rolls-royce-wraith-white.jpg"/>
                </div>
                <div class="back">
                    <p class="carInfo">2016 Rolls Royce Wraith<br>100km on clock<br>$235,416</p>
                </div>
            </div>
        </div>
        <div class="table-cell">
            <div class="flipcard v">
                <div class="front">
                    <img class="featuredCars" src="../Cars%20For%20Sale/Mercedes_SLS.jpg"/>
                </div>
                <div class="back">
                    <p class="carInfo">2013 Mercedes-Benz SLS AMG Roadster<br>12,000km on clock<br>$250,000</p>
                </div>
            </div>
        </div>
        <div class="table-cell">
            <div class="flipcard v">
                <div class="front">
                    <img class="featuredCars" src="../Cars%20For%20Sale/huracanblack.jpg"/>
                </div>
                <div class="back">
                    <p class="carInfo">2015 Lamborghini Huracan LP 610<br>5000km on clock<br>$300,000</p>
                </div>
            </div>
        </div>
    </div>
</div>
<footer class="tweakedFooter">
    <div>
        <div id="footer-navigation-bar-shop-for-a-car">
            <div id="shopForACar"><span>Shop for a Car</span></div>
            <div class="footer-navigation-button"><a href="Buy.php">Cars For Sale</a></div>
            <div class="footer-navigation-button"><a href="Buy.php">Car Deals</a></div>
            <div class="footer-navigation-button"><a href="Buy.php">New Listings</a></div>
        </div>

        <div id="footer-navigation-my-account">
            <div id="my-account"><span>My Account</span></div>
            <div class="footer-navigation-button"><a href="<?php if ($accessToSell == true) {
                    echo 'My%20Account.php';
                } ?>">Your Listings</a></div>
            <div class="footer-navigation-button"><a href="">Watching</a></div>
            <div class="footer-navigation-button"><a href="<?php if ($accessToSell == true) {
                    echo 'Sell.php';
                } ?>">Sell A Car</a></div>
        </div>

        <div id="footer-navigation-about-us">
            <div id="about-us"><span>About Us</span></div>
            <div class="footer-navigation-button"><a href="www.weltec.ac.nz">Company Information</a></div>
            <div class="footer-navigation-button"><a href="www.weltec.ac.nz">Corporate Information</a></div>
            <div class="footer-navigation-button"><a href="Contact%20Us.html">Contact Us</a></div>
        </div>

        <div id="footer-navigation-research-cars">
            <div id="research-cars"><span>Research Cars</span></div>
            <div class="footer-navigation-button"><a href="Buy.php">Car Research</a></div>
            <div class="footer-navigation-button"><a href="Buy.php">New Cars</a></div>
            <div class="footer-navigation-button"><a href="Buy.php">Used Cars</a></div>
        </div>
    </div>
</footer>
</body>
</html>