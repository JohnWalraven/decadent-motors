<?php

/**
 * Created by PhpStorm.
 * User: JWalraven
 * Date: 3/05/2016
 * Time: 10:48 AM
 */
session_start();
if (!isset($_SESSION["emailAddress"]) && !isset($_SESSION["password"])) {
    $loginStatus = "Currently not logged in";
    $accessToSell = false;
    $accessToAccount = false;
} else {
    $loginStatus = "Logged in as " . (isset($_SESSION['name']) ? $_SESSION['name'] : '') . "<input type='submit' value='Sign out'/>";
    $accessToSell = true;
    $accessToAccount = true;
}
$image;
if ($_FILES["file"]["size"] < 800000) {

    if ($_FILES["file"]["error"] > 0) {
        echo "Error:" . $_FILES["file"]["error"] . "<br>";
    } else {

        if (file_exists("upload/" . $_FILES["file"]["name"])) {
            echo $_FILES["file"]["name"] . "already exists.";
        } else {
            move_uploaded_file($_FILES["file"]["tmp_name"], "upload/" . $_FILES["file"]["name"]);
            $image = "upload/" . $_FILES["file"]["name"];
        }
    }
} else {
    echo "There was an error uploading the file";
}
if (isset($_POST['inputSubmit'])) {
    $fName = $_POST['inputFirstName'];
    $lName = $_POST['inputLastName'];
    $email = $_POST['inputEmail'];
    $userPassword = md5($_POST['inputPassword']);
    $userName = $_POST['inputUsername'];
    $location = $_POST['inputLocation'];

    $servername = "localhost:3306";
    $dbUsername = "root";
    $dbPassword = "";
    $dbname = "decadentmotortrade";

    $connection = mysqli_connect($servername, $dbUsername, $dbPassword, $dbname);

    if ($connection->connect_error) {
        die("Connection failed: " . $connection->connect_error);
    }


    $stmt = "INSERT INTO account (Account_ID, Account_FName, Account_LName, Account_Username, Account_Password, Account_EmailAddress, Account_Location, Account_Photo) 
VALUES (Default,'{$fName}','{$lName}','{$userName}','{$userPassword}','{$email}','{$location}','{$image}');";
    $_SESSION["emailAddress"] = $userPassword;
    $_SESSION["password"] = $enteredPassword;

    if ($connection->query($stmt) === TRUE) {
        $status = "Account created successfully click " . "<p><a href='My Account.php'> here " . "visit your account.";
    } else {
        $status = "An error has occurred please try again";
    }
    $connection->close();
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Decadent Motor Trade</title>
    <link rel="stylesheet" type="text/css" href="../CSS/Theme.css">
    <script src="../Javascript/JavaScriptFile.js"></script>
</head>

<header>
    <div id="signOut">
        <form method="post" action="Logout.php">
            <label><?php echo $loginStatus; ?></label>
        </form>
    </div>
    <div id="signIn">
        <form method="post" action="login.php">
            <input type="text" id="loginEmailAddress" name="inputLoginEmailAddress"
                   placeholder="Email Address or Username"
                   onblur="testValidity('loginEmailAddress','requiredLoginEmailAddress' )">
            <input type="password" id="loginPassword" name="inputLoginPassword" placeholder="Password"
                   onblur="testValidity('loginPassword','requiredLoginPassword' )">
            <input type="submit" name="submitDetails" value="Sign In" onclick="validateLogin()"><br>
            <label class="requiredField" id="requiredLoginEmailAddress">This field is required</label>
            <label class="requiredField" id="requiredLoginPassword" style="padding-left:6%; ">This field is
                required</label>
        </form>
    </div>
    <div id="header">
        <div id="logo-div">
            <a href="Home Page.php"><img class="logo-picture" src="../Images/Drawing.png"/></a>
        </div>
        <div id="navigation-bar">
            <div class="navigation-button"><a href="Buy.php">Buy</a></div>
            <div class="navigation-button"><a href="<?php if ($accessToSell == true) {
                    echo 'Sell.php';
                } ?>">Sell</a></div>
            <div class="navigation-button"><a href="<?php if ($accessToSell == true) {
                    echo 'My%20Account.php';
                } ?>">My Account</a></div>
            <div class="navigation-button"><a href="Feedback.php">Feedback</a></div>
            <div class="navigation-button"><a href="About%20US.php">About Us</a></div>
        </div>
    </div>
</header>
<body>
<h2><?php echo $status; ?></h2>
<footer>
    <div>
        <div id="footer-navigation-bar-shop-for-a-car">
            <div id="shopForACar"><span>Shop for a Car</span></div>
            <div class="footer-navigation-button"><a href="Buy.php">Cars For Sale</a></div>
            <div class="footer-navigation-button"><a href="Buy.php">Car Deals</a></div>
            <div class="footer-navigation-button"><a href="Buy.php">New Listings</a></div>
        </div>

        <div id="footer-navigation-my-account">
            <div id="my-account"><span>My Account</span></div>
            <div class="footer-navigation-button"><a href="<?php if ($accessToSell == true) {
                    echo 'My%20Account.php';
                } ?>">Your Listings</a></div>
            <div class="footer-navigation-button"><a href="">Watching</a></div>
            <div class="footer-navigation-button"><a href="<?php if ($accessToSell == true) {
                    echo 'Sell.php';
                } ?>">Sell A Car</a></div>
        </div>

        <div id="footer-navigation-about-us">
            <div id="about-us"><span>About Us</span></div>
            <div class="footer-navigation-button"><a href="http://www.weltec.ac.nz">Company Information</a></div>
            <div class="footer-navigation-button"><a href="http://www.weltec.ac.nz">Corporate Information</a></div>
            <div class="footer-navigation-button"><a href="Contact%20Us.php">Contact Us</a></div>
        </div>

        <div id="footer-navigation-research-cars">
            <div id="research-cars"><span>Research Cars</span></div>
            <div class="footer-navigation-button"><a href="Buy.php">Car Research</a></div>
            <div class="footer-navigation-button"><a href="Buy.php">New Cars</a></div>
            <div class="footer-navigation-button"><a href="Buy.php">Used Cars</a></div>
        </div>
    </div>
</footer>
</body>
</html>