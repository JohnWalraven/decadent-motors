<?php
session_start();
if (!isset($_SESSION["emailAddress"]) && !isset($_SESSION["password"])) {
    $loginStatus = "Currently not logged in";
    $accessToSell = false;
    $accessToAccount = false;
} else {
    $loginStatus = "Logged in as " . (isset($_SESSION['name']) ? $_SESSION['name'] : '') . "<input type='submit' value='Sign out'/>";
    $accessToSell = true;
    $accessToAccount = true;
}
$serverName = "localhost:3306";
$dbUsername = "root";
$dbPassword = "";
$dbName = "decadentmotortrade";

$connection = mysqli_connect($serverName, $dbUsername, $dbPassword, $dbName);

if ($connection->connect_error) {
    die("Connection failed: " . $connection->connect_error);
}

$resultCarsBeingSold = mysqli_query($connection, "SELECT Car_Make, Car_Model, Car_Price FROM cars ORDER BY Car_Price ASC;");

while ($rowCar = mysqli_fetch_array($resultCarsBeingSold, MYSQL_ASSOC)) {
    $Car_Make[] = "<option>" . $rowCar["Car_Make"] . "</option>";
    $Car_Model[] = "<option>" . $rowCar["Car_Model"] . "</option>";
    $Car_Price[] = "<option>" . $rowCar["Car_Price"] . "</option>";
}
$connection->close();
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Decadent Motor Trade</title>
    <link rel="stylesheet" type="text/css" href="../CSS/Theme.css">
    <script src="../Javascript/JavaScriptFile.js"></script>
</head>

<header>
    <div id="signOut">
        <form method="post" action="Logout.php">
            <label><?php echo $loginStatus; ?></label>
        </form>
    </div>
    <div id="signIn">
        <form method="post" action="login.php">
            <input type="text" id="loginEmailAddress" name="inputLoginEmailAddress" placeholder="Email Address"
                   onblur="testValidity('loginEmailAddress','requiredLoginEmailAddress' )">
            <input type="password" id="loginPassword" name="inputLoginPassword" placeholder="Password"
                   onblur="testValidity('loginPassword','requiredLoginPassword' )">
            <input type="submit" name="submitDetails" value="Sign In" onclick="validateLogin()"><br>
            <label class="requiredField" id="requiredLoginEmailAddress">This field is required</label>
            <label class="requiredField" id="requiredLoginPassword" style="padding-left:6%; ">This field is
                required</label>
        </form>
    </div>
    <div id="header">
        <div id="logo-div">
            <a href="Home Page.php"><img class="logo-picture" src="../Images/Drawing.png"/></a>
        </div>
        <div id="navigation-bar">
            <div class="navigation-button"><a href="Buy.php">Buy</a></div>
            <div class="navigation-button"><a href="<?php if ($accessToSell == true) {
                    echo 'Sell.php';
                } ?>">Sell</a></div>
            <div class="navigation-button"><a href="<?php if ($accessToSell == true) {
                    echo 'My%20Account.php';
                } ?>">My Account</a></div>
            <div class="navigation-button"><a href="Feedback.php">Feedback</a></div>
            <div class="navigation-button"><a href="About%20US.php">About Us</a></div>
        </div>
    </div>
</header>

<body>
<section>
    <div id="ferrari"></div>
    <div id="paragraphAboutUs">
        <p id="aboutUs">We buy or sell some of the worlds most<br>
            beautiful vehicles to the most<br>
            discerning lovers of cars. </p>
    </div>
    <div id="pagani"></div>

    <div id="carSelectorBox">
        <form action="searchResultsHomePage.php" method="post">
            <div class="carSelectBox">
                <div>
                    <label>Find your car your way</label>
                </div>
            </div>
            <div class="carSelectBox">
                <select class="find-your-car-select-box" name="searchCarMake">
                    <option>Any Make</option>
                    <?php foreach ($Car_Make as $value) {
                        echo $value;
                    } ?>
                </select>
            </div>

            <div class="carSelectBox">
                <select class="find-your-car-select-box" name="searchCarModel">
                    <option>Any Model</option>
                    <?php foreach ($Car_Model as $value) {
                        echo $value;
                    } ?>
                </select>
            </div>

            <div class="carSelectBox">
                <select class="find-your-car-select-box" name="searchMinimumPrice">
                    <option>Any Price</option>
                    <?php foreach ($Car_Price as $value) {
                        echo $value;
                    } ?>
                </select>
            </div>

            <div>
                <input class="submitButton" type="submit" name="Submit"/>
            </div>
        </form>
    </div>
    <div id="signUpForm">
        <span id="signUpSpan">Sign Up</span><br>
        <label style="font-size: 10pt;margin-left: 2%;">Fields marked with an asterisk are required.</label><br>

        <form action="DatabaseHomepage.php" method="post" enctype="multipart/form-data">
            <input class="signUpTextField" type="text" id="firstName" name="inputFirstName" placeholder="*First Name:"
                   onblur="testValidity('firstName','requiredFirstName' )">
            <input class="signUpTextField" type="text" id="lastName" name="inputLastName" placeholder="*Last Name:"
                   onblur="testValidity('lastName', 'requiredLastName')">

            <label class="requiredField" id="requiredLastName">This field is required</label><br>
            <label class="requiredField" id="requiredFirstName">This field is required</label><br>

            <input class="signUpTextField" type="text" id="emailAddress" name="inputEmail" placeholder="*Email Address"
                   onblur="testValidity('emailAddress', 'requiredEmailAddress')">
            <input class="signUpTextField" type="text" id="confirmEmailAddress" name="inputConfirmEmail"
                   placeholder="*Confirm Email Address">
            <label class="requiredField" id="requiredConfirmEmailAddress">This field is required</label><br>
            <label class="requiredField" id="requiredEmailAddress">This field is required</label><br>

            <input class="signUpTextField" type="password" id="password" name="inputPassword" placeholder="*Password"
                   onblur="testValidity('password', 'requiredPassword')">
            <input class="signUpTextField" type="password" id="confirmPassword" name="inputConfirmPasword"
                   placeholder="*Confirm Password">

            <label class="requiredField" id="requiredConfirmPassword">This field is required</label><br>
            <label class="requiredField" id="requiredPassword">This field is required</label><br>

            <input class="signUpTextField" type="text" id="Username" name="inputUsername" placeholder="*Username"
                   onblur="testValidity('Username', 'requiredUsername')">
            <input class="signUpTextField" type="text" id="location" name="inputLocation" placeholder="*Location"
                   onblur="testValidity('location', 'requiredLocation')"><br>

            <label class="requiredField" id="requiredUsername">This field is required</label>
            <label class="requiredField" id="requiredLocation">This field is required</label><br>
            Upload a profile photo:
            <input type="file" name="file" id="file">
            <br/>
            <input class="signUpTextField" type="checkBox" id="termsAndConditions">
            *I accept the terms of the Privacy Statement and Visitor Agreement.<br>
            <label class="requiredField" id="requiredTerms">This field is required</label><br>
            <input class="submitButton" type="submit" id="submitDetails" name="inputSubmit"
                   onclick="validateHomePage()">
        </form>
    </div>
</section>
<footer>
    <div>
        <div id="footer-navigation-bar-shop-for-a-car">
            <div id="shopForACar"><span>Shop for a Car</span></div>
            <div class="footer-navigation-button"><a href="Buy.php">Cars For Sale</a></div>
            <div class="footer-navigation-button"><a href="Buy.php">Car Deals</a></div>
            <div class="footer-navigation-button"><a href="Buy.php">New Listings</a></div>
        </div>

        <div id="footer-navigation-my-account">
            <div id="my-account"><span>My Account</span></div>
            <div class="footer-navigation-button"><a href="<?php if ($accessToSell == true) {
                    echo 'My%20Account.php';
                } ?>">Your Listings</a></div>
            <div class="footer-navigation-button"><a href="">Watching</a></div>
            <div class="footer-navigation-button"><a href="<?php if ($accessToSell == true) {
                    echo 'Sell.php';
                } ?>">Sell A Car</a></div>
        </div>

        <div id="footer-navigation-about-us">
            <div id="about-us"><span>About Us</span></div>
            <div class="footer-navigation-button"><a href="http://www.weltec.ac.nz">Company Information</a></div>
            <div class="footer-navigation-button"><a href="http://www.weltec.ac.nz">Corporate Information</a></div>
            <div class="footer-navigation-button"><a href="Contact%20Us.php">Contact Us</a></div>
        </div>

        <div id="footer-navigation-research-cars">
            <div id="research-cars"><span>Research Cars</span></div>
            <div class="footer-navigation-button"><a href="Buy.php">Car Research</a></div>
            <div class="footer-navigation-button"><a href="Buy.php">New Cars</a></div>
            <div class="footer-navigation-button"><a href="Buy.php">Used Cars</a></div>
        </div>
    </div>
</footer>
</body>
</html>