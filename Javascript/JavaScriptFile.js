/**
 * Created by im_a_ on 5/04/2016.
 */
function validateHomePage() {

    if (document.getElementById("firstName").value == "" || document.getElementById("firstName").value == null) {
        document.getElementById("requiredFirstName").style.visibility = "visible";
    }

    if (document.getElementById("lastName").value == "" || document.getElementById("lastName").value == null) {
        document.getElementById("requiredLastName").style.visibility = "visible";
    }

    if (document.getElementById("emailAddress").value == "" || document.getElementById("emailAddress").value == null) {
        document.getElementById("requiredEmailAddress").style.visibility = "visible";
    }

    if (document.getElementById("confirmEmailAddress").value == "" || document.getElementById("confirmEmailAddress").value == null) {
        document.getElementById("requiredConfirmEmailAddress").style.visibility = "visible";
    }

    if (document.getElementById("password").value == "" || document.getElementById("password").value == null) {
        document.getElementById("requiredPassword").style.visibility = "visible";
    }

    if (document.getElementById("confirmPassword").value == "" || document.getElementById("confirmPassword").value == null) {
        document.getElementById("requiredConfirmPassword").style.visibility = "visible";
    }

    if (document.getElementById("Username").value == "" || document.getElementById("Username").value == null) {
        document.getElementById("requiredUsername").style.visibility = "visible";
    }

    if (document.getElementById("termsAndConditions").checked == false) {
        document.getElementById("requiredTerms").style.visibility = "visible";
    }

}
function validateLogin() {
    if (document.getElementById("loginEmailAddress").value == "" || document.getElementById("loginEmailAddress").value == null) {
        document.getElementById("requiredLoginEmailAddress").style.visibility = "visible";
    }

    if (document.getElementById("loginPassword").value == "" || document.getElementById("loginPassword").value == null) {
        document.getElementById("requiredLoginPassword").style.visibility = "visible";
    }
}
function validateSellPage() {
    if (document.getElementById("carLocation").value == "" || document.getElementById("carLocation").value == null) {
        document.getElementById("requiredSellRegion").style.visibility = "visible";
    }

    if (document.getElementById("kilometers").value == "" || document.getElementById("kilometers").value == null) {
        document.getElementById("requiredKilometers").style.visibility = "visible";
    }

    if (document.getElementById("carPrice").value == "" || document.getElementById("carPrice").value == null) {
        document.getElementById("requiredCarPrice").style.visibility = "visible";
    }

    if (document.getElementById("sellCarModel").value == "" || document.getElementById("sellCarModel").value == null) {
        document.getElementById("requiredCarModel").style.visibility = "visible";
    }

    if (document.getElementById("sellCarMake").value == "" || document.getElementById("sellCarMake").value == null) {
        document.getElementById("requiredCarMake").style.visibility = "visible";
    }

    if (document.getElementById("sellCarBodyStyle").value == "" || document.getElementById("sellCarBodyStyle").value == null) {
        document.getElementById("requiredCarBodyStyle").style.visibility = "visible";
    }

    if (document.getElementById("sellFormPhoneNumber").value == "" || document.getElementById("sellFormPhoneNumber").value == null) {
        document.getElementById("requiredSellPhoneNumber").style.visibility = "visible";
    }

    if (document.getElementById("sellFormEmailAddress").value == "" || document.getElementById("sellFormEmailAddress").value == null) {
        document.getElementById("requiredSellEmailAddress").style.visibility = "visible";
    }

    if (document.getElementById("sellFromConfirmEmailAddress").value == "" || document.getElementById("sellFromConfirmEmailAddress").value == null) {
        document.getElementById("requiredSellConfirmEmailAddress").style.visibility = "visible";
    }

    if (document.getElementById("sellCarYear").value == "" || document.getElementById("sellCarYear").value == null) {
        document.getElementById("requiredCarYear").style.visibility = "visible";
    }
}

function validateFeedBack() {
    if (document.getElementById("feedbackName").value == "" || document.getElementById("feedbackName").value == null) {
        document.getElementById("requiredFeedBackPhoneNumber").style.visibility = "visible";
    }

    if (document.getElementById("feedbackEmail").value == "" || document.getElementById("feedbackEmail").value == null) {
        document.getElementById("requiredFeedBackEmail").style.visibility = "visible";
    }

    if (document.getElementById("feedbackPhone").value == "" || document.getElementById("feedbackPhone").value == null) {
        document.getElementById("requiredSellPhoneNumber").style.visibility = "visible";
    }

    if (document.getElementById("FeedbackComments").value == "" || document.getElementById("FeedbackComments").value == null) {
        document.getElementById("requiredFeedBackComments").style.visibility = "visible";
    }

}
function testValidity(field, requiredLabel) {

    var valueOfField = document.getElementById(field);
    var valueOfRequiredLabel = document.getElementById(requiredLabel);
    var emailPattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var namePattern = /^[a-zA-Z ]+$/;
    var homePhone = /^(\([0-9]{3}\)|[0-9]{3}-)[0-9]{3}-[0-9]{4} | ^02[0-7][ ]?[0-9]{3}[ ]?[0-9]{4}$/;
    var passwordPattern = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
    var pricePattern = /^[0-9]+.?[0-9]+$/;
    var usernamePattern = /^[a-z A-z 0-9]{6,}$/;
    var kilometersPattern = /^[0-9]+$/;
    var carYearPattern = /^(19|20)\d{2}$/;
    var matches;

    switch (field) {
        case 'emailAddress':
            matches = emailPattern;
            document.getElementById("requiredEmailAddress").textContent = "^The email address format is invalid";
            break;

        case 'Username':
            matches = usernamePattern;
            document.getElementById("requiredUsername").textContent = "^The username must be more than 6 characters";
            break;

        case 'firstName':
            matches = namePattern;
            document.getElementById("requiredFirstName").textContent = "^The first name contains invalid characters";
            break;

        case 'lastName':
            matches = namePattern;
            document.getElementById("requiredLastName").textContent = "<Contains invalid characters";
            break;

        case 'password':
            matches = passwordPattern;
            document.getElementById("requiredPassword").textContent = "^The password requires " +
                "one digit, one upper and lower case and must be more than 8 characters";
            break;

        case 'loginEmailAddress':
            matches = emailPattern;
            document.getElementById("requiredLoginEmailAddress").textContent = "^The email address format is invalid";
            break;

        case 'loginPassword':
            matches = passwordPattern;
            document.getElementById("requiredLoginPassword").textContent = "^Password is invalid";
            break;

        case 'location':
            matches = namePattern;
            document.getElementById("requiredLocation").textContent = "^Location is invalid"
            break;

        case 'carLocation':
            matches = namePattern;
            document.getElementById("requiredSellRegion").textContent = "<Invalid characters found";
            break;

        case 'sellCarYear':
            matches = carYearPattern;
            document.getElementById("requiredCarYear").textContent = "<Year of car invalid";
            break;

        case 'sellCarMake':
            matches = namePattern;
            document.getElementById("requiredCarMake").textContent = "<Make of car is invalid";
            break;

        case 'sellCarBodyStyle':
            matches = namePattern;
            document.getElementById("requiredCarBodyStyle").textContent = "<Body style is invalid";
            break;

        case 'kilometers':
            matches = kilometersPattern;
            document.getElementById("requiredKilometers").textContent = "<Invalid characters found";
            break;

        case 'carPrice':
            matches = pricePattern;
            document.getElementById("requiredCarPrice").textContent = "<Invalid characters found";
            break;

        case 'sellFormPhoneNumber':
            // matches = homePhone;
            document.getElementById("requiredSellPhoneNumber").textContent = "<Phone number is invalid";
            break;

        case 'sellFormEmailAddress':
            matches = emailPattern;
            document.getElementById("sellFormEmailAddress").textContent = "<Email address is invalid";
            break;

        case 'feedbackName':
            matches = namePattern;
            document.getElementById("requiredFeedBackPhoneNumber").textContent = "^Name is invalid";
            break;

        case 'feedbackEmail':
            matches = emailPattern;
            document.getElementById("requiredFeedBackEmail").textContent = "^Email is invalid";
            break;

        case 'feedbackPhone':
            matches = homePhone;
            document.getElementById("requiredSellPhoneNumber").textContent = "^Phone number is invalid";
            break;
    }
    if (!matches.test(valueOfField.value)) {
        valueOfRequiredLabel.style.visibility = "visible";
        return false;
    }
    else
        valueOfRequiredLabel.style.visibility = "hidden";

}

function initMap() {
    var myLatLng = {lat: 34.067054, lng: -118.388532};
    var map = new google.maps.Map(document.getElementById("map"), {
        zoom: 9,
        center: myLatLng
    });
    var mapDiv = new google.maps.Map(document.getElementById("map"), map);
    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        title: "Decadent Motor Trade"
    });
}