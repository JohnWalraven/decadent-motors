<?php

/**
 * Created by PhpStorm.
 * User: JWalraven
 * Date: 3/05/2016
 * Time: 10:48 AM
 */
session_start();
if (!isset($_SESSION["emailAddress"]) && !isset($_SESSION["password"])) {
    $loginStatus = "Currently not logged in";
    $accessToSell = false;
    $accessToAccount = false;
    header('Location:Home%20Page.php');
} else {
    $loginStatus = "Logged in as " . (isset($_SESSION['name']) ? $_SESSION['name'] : '') . "<input type='submit' value='Sign out'/>";
    $accessToSell = true;
    $accessToAccount = true;
}

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Decadent Motor Trade</title>
    <link rel="stylesheet" type="text/css" href="../CSS/Theme.css">
    <script src="../Javascript/JavaScriptFile.js"></script>
</head>

<header>
    <div id="signOut">
        <form method="post" action="Logout.php">
            <label><?php echo $loginStatus; ?></label>
        </form>
    </div>
    <div id="signIn">
        <form method="post" action="login.php">
            <input type="text" id="loginEmailAddress" name="inputLoginEmailAddress"
                   placeholder="Email Address or Username"
                   onblur="testValidity('loginEmailAddress','requiredLoginEmailAddress' )">
            <input type="password" id="loginPassword" name="inputLoginPassword" placeholder="Password"
                   onblur="testValidity('loginPassword','requiredLoginPassword' )">
            <input type="submit" name="submitDetails" value="Sign In" onclick="validateLogin()"><br>
            <label class="requiredField" id="requiredLoginEmailAddress">This field is required</label>
            <label class="requiredField" id="requiredLoginPassword" style="padding-left:6%; ">This field is
                required</label>
        </form>
    </div>
    <div id="header">
        <div id="logo-div">
            <a href="Home%20Page.php"><img class="logo-picture" src="../Images/Drawing.png"/></a>
        </div>
        <div id="navigation-bar">
            <div class="navigation-button"><a href="Buy.php">Buy</a></div>
            <div class="navigation-button"><a href="Sell.php">Sell</a></div>
            <div class="navigation-button"><a href="My%20Account.php">My Account</a></div>
            <div class="navigation-button"><a href="Feedback.php">Feedback</a></div>
            <div class="navigation-button"><a href="About%20US.php">About Us</a></div>
        </div>
    </div>
</header>
<body>
<div id="Form-Sell-Car">
    <form action="DatabaseSell.php" method="post" enctype="multipart/form-data">

        <span class="FindYourCar">Vehicle Details </span> <br><br>
        <p class="VehicleDetails">Enter the vehicle information that you wish to be displayed to potential buyers
            on your ad.<br> "Vehicle information and "Contact Information are the only required fields,<br>
            but the more information you add the better exposure you get.<br>
            Fields marked with an asterisk are required.
        </p>

        <label class="SellCarFormLabels" id="carLocationLabel">*Region Where Car will be sold</label><br>
        <input type="text" id="carLocation" name="inputSellCarLocation" placeholder="e.g. Wellington" on
               onblur="testValidity('carLocation', 'requiredSellRegion')">
        <label class="requiredField" id="requiredSellRegion">This field is required</label><br>

        <label class="SellCarFormLabels">*Year</label><br>
        <input type="text" id="sellCarYear" name="inputSellCarYear" class="sellCarSelectBox" placeholder="e.g. 2001"
               onblur="testValidity('sellCarYear', 'requiredCarYear')">
        <label class="requiredField" id="requiredCarYear">This field is required</label><br>

        <label class="SellCarFormLabels">*Make </label><br>
        <input type="text" id="sellCarMake" name="inputSellCarMake" class="sellCarSelectBox" placeholder="e.g. Ferrari"
               onblur="testValidity('sellCarMake', 'requiredCarMake')">
        <label class="requiredField" id="requiredCarMake">This field is required</label><br>

        <label class="SellCarFormLabels"> *Model </label><br>
        <input type="text" id="sellCarModel" name="inputSellCarModel" class="sellCarSelectBox"
               placeholder="e.g. Aventador">
        <label class="requiredField" id="requiredCarModel">This field is required</label><br>

        <label class="SellCarFormLabels">*Body Style</label><br>
        <input type="text" id="sellCarBodyStyle" name="inputSellCarBodyStyle" class="sellCarSelectBox"
               placeholder="e.g. Convertible" onblur="testValidity('sellCarBodyStyle','requiredCarBodyStyle')">
        <label class="requiredField" id="requiredCarBodyStyle">This field is required</label><br>

        <label class="SellCarFormLabels" id="kilometersLabel">*Kilometers</label><br>
        <input type="text" id="kilometers" name="inputSellKilometers"
               onblur="testValidity('kilometers', 'requiredKilometers')">
        <label class="requiredField" id="requiredKilometers">This field is required</label><br>

        <label class="SellCarFormLabels" id="priceLabel">*Price</label><br>
        <input type="text" id="carPrice" name="inputSellCarPrice" onblur="testValidity('carPrice', 'requiredCarPrice')">
        <label class="requiredField" id="requiredCarPrice">This field is required</label><br>

        <p class="VehicleDetails">This is your chance to tell any prospective buyers about your vehicle<br>
            in your own words, We've already captured your year, model and make so focus on the unique points<br>
            and explain why you're selling and warranty information.</p>
        <label class="SellCarFormLabels">Seller Comments </label><br>
        <textarea id="sellerComments" name="inputSellCarComments" rows="5" cols="70"></textarea><br><br><br>

        <span class="FindYourCar">Contact Details</span><br><br>
        <p class="VehicleDetails"> Enter the phone number ans email address you want to use for your ad<br>
            your phone number will be available to prospective buyers.<br>
            We will email you notifications and information, but will never display your email address.<br><br>
            By providing us with your phone number, you acknowledge and agree that we (and/our service providers)<br>
            may contact you at the mobile or other number you list.</p>

        <label class="SellCarFormLabels">*Phone Number</label><br>
        <input type="text" id="sellFormPhoneNumber" name="inputSellCarPhoneNumber"
               onblur="testValidity('sellFormPhoneNumber', 'requiredSellPhoneNumber')">
        <label class="requiredField" id="requiredSellPhoneNumber">This field is required</label><br><br>

        <label class="SellCarFormLabels">*Email Address</label><br>
        <input type="text" id="sellFormEmailAddress" name="inputSellCarEmailAddress"
               onblur="testValidity('sellFormEmailAddress', 'requiredSellEmailAddress')">
        <label class="requiredField" id="requiredSellEmailAddress">This field is required</label><br><br>

        <label class="SellCarFormLabels">*Confirm Email Address</label><br>
        <input type="text" id="sellFromConfirmEmailAddress">
        <label class="requiredField" id="requiredSellConfirmEmailAddress">This field is required</label><br><br>
        <span class="FindYourCar">Upload Photos</span><br><br>
        FileName:<input type="file" name="file" id="file"><br/>

        <input type="submit" id="submitCarDetails" name="inputSubmitDetails" value="Submit and upload pictures"
               onclick="validateSellPage()">
    </form>
</div>
<footer>
    <div>
        <div id="footer-navigation-bar-shop-for-a-car">
            <div id="shopForACar"><span>Shop for a Car</span></div>
            <div class="footer-navigation-button"><a href="Buy.php">Cars For Sale</a></div>
            <div class="footer-navigation-button"><a href="Buy.php">Car Deals</a></div>
            <div class="footer-navigation-button"><a href="Buy.php">New Listings</a></div>
        </div>

        <div id="footer-navigation-my-account">
            <div id="my-account"><span>My Account</span></div>
            <div class="footer-navigation-button"><a href="My%20Account.php">Your Listings</a></div>
            <div class="footer-navigation-button"><a href="">Watching</a></div>
            <div class="footer-navigation-button"><a href="Sell.html">Sell A Car</a></div>
        </div>

        <div id="footer-navigation-about-us">
            <div id="about-us"><span>About Us</span></div>
            <div class="footer-navigation-button"><a href="www.weltec.ac.nz">Company Information</a></div>
            <div class="footer-navigation-button"><a href="www.weltec.ac.nz">Corporate Information</a></div>
            <div class="footer-navigation-button"><a href="Contact%20Us.html">Contact Us</a></div>
        </div>

        <div id="footer-navigation-research-cars">
            <div id="research-cars"><span>Research Cars</span></div>
            <div class="footer-navigation-button"><a href="Buy.php">Car Research</a></div>
            <div class="footer-navigation-button"><a href="Buy.php">New Cars</a></div>
            <div class="footer-navigation-button"><a href="Buy.php">Used Cars</a></div>
        </div>
    </div>
</footer>
</body>
</html>