<?php

/**
 * Created by PhpStorm.
 * User: JWalraven
 * Date: 17/05/2016
 * Time: 7:49 PM
 */
$serverName = "localhost:3306";
$dbUsername = "root";
$dbPassword = "";
$dbName = "decadentmotortrade";

$listingToBeDeleted = $_POST["Listing"];

$connection = mysqli_connect($serverName, $dbUsername, $dbPassword, $dbName);

if ($connection->connect_error) {
    die("Connection failed: " . $connection->connect_error);
}

$stmt = "DELETE from cars WHERE Car_ID = '{$listingToBeDeleted[0]}' LIMIT 1;";

if ($connection->query($stmt) === TRUE) {
    echo "Delete successful";
} else {
    echo "An error has occurred please try again";
}

header('Location:My Account.php');
$connection->close();
?>