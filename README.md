# Decadent Motors #
![Capture.PNG](https://bitbucket.org/repo/K8aXxX/images/830978898-Capture.PNG)

This is a project that was done for an assignment during my degree.

The task for the assignment was to create a site for the purpose of buying and selling cars using many different technologies to achieve it.

The functionality allowed a user to create an account and list a car as well as search for a car to buy, this was done by using a MYSQL database to store account details and car listings. The front end was created entirely by hand using HTML and CSS, without the use of a template, this is supported by Javascript to test the inputs from the user to prevent un-suitable data from being sent to the database. The front end is backed by PHP, this was selected so that I could take the listed information and store the information in the database.

Technologies used:
   HTML 5
   CSS 3
   PHP
   Wamp Server
   PHPStorm
   Google Maps JavaScript API
   CSS 3D Animations

I achieved a marl of 99% or A+ for this website.