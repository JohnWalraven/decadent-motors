<?php
/**
 * Created by PhpStorm.
 * User: JWalraven
 * Date: 16/05/2016
 * Time: 1:08 PM
 */
session_start();
if (!isset($_SESSION["emailAddress"]) && !isset($_SESSION["password"])) {
    $loginStatus = "Currently not logged in";
    $accessToSell = false;
    $accessToAccount = false;
} else {
    $accessToSell = true;
    $accessToAccount = true;

    $loginStatus = "Logged in as " . $_SESSION["name"] . "<input type='submit' value='Sign out'/>";
    $serverName = "localhost:3306";
    $dbUsername = "root";
    $dbPassword = "";
    $dbName = "decadentmotortrade";

    $accountEmailAddress = $_SESSION['emailAddress'];
    $userID = $_SESSION['userId'];

    $connection = mysqli_connect($serverName, $dbUsername, $dbPassword, $dbName);

    if ($connection->connect_error) {
        die("Connection failed: " . $connection->connect_error);
    }

    $resultAccountDetails = mysqli_query($connection, "SELECT * FROM account WHERE Account_EmailAddress = '{$accountEmailAddress}';");
    $resultCarsBeingSold = mysqli_query($connection, "SELECT * FROM cars WHERE ACCOUNT_REFERENCE = '{$userID}';");

    if (mysqli_num_rows($resultAccountDetails) > 0) {

        $row = mysqli_fetch_assoc($resultAccountDetails);
        $username = $row["Account_Username"];
        $name = $row["Account_FName"] . $row["Account_LName"];
        $_SESSION["name"] = $name;
        $email = $row["Account_EmailAddress"];
        $location = $row["Account_Location"];
        $profileImage = $row["Account_Photo"];

    }
    if (mysqli_num_rows($resultCarsBeingSold) > 0) {

        while ($rowCar = mysqli_fetch_array($resultCarsBeingSold)) {
            $Car_name[] = "Name: " . $rowCar["Car_Make"] . " " . $rowCar["Car_Model"];
            $Car_Price[] = "Price: $" . $rowCar["Car_Price"];
            $Car_Kilometers[] = "KM's on clock: " . $rowCar["Car_Kilometers"] . "Km";
            $Car_Image[] = $rowCar["CAR_PHOTOS"];
            $Car_id[] = $rowCar["Car_ID"];
        }

    }


    $connection->close();
}
?>


<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Decadent Motor Trade</title>
    <link rel="stylesheet" type="text/css" href="../CSS/Theme.css">
    <script src="../Javascript/JavaScriptFile.js"></script>
</head>

<header>
    <div id="signOut">
        <form method="post" action="Logout.php">
            <label><?php echo $loginStatus; ?></label>
        </form>
    </div>
    <div id="signIn">
        <form method="post" action="login.php">
            <input type="text" id="loginEmailAddress" name="inputLoginEmailAddress"
                   placeholder="Email Address or Username"
                   onblur="testValidity('loginEmailAddress','requiredLoginEmailAddress' )">
            <input type="password" id="loginPassword" name="inputLoginPassword" placeholder="Password"
                   onblur="testValidity('loginPassword','requiredLoginPassword' )">
            <input type="submit" name="submitDetails" value="Sign In" onclick="validateLogin()"><br>
            <label class="requiredField" id="requiredLoginEmailAddress">This field is required</label>
            <label class="requiredField" id="requiredLoginPassword" style="padding-left:6%; ">This field is
                required</label>
        </form>
    </div>
    <div id="header">
        <div id="logo-div">
            <a href="Home%20Page.php"><img class="logo-picture" src="../Images/Drawing.png"/></a>
        </div>
        <div id="navigation-bar">
            <div class="navigation-button"><a href="Buy.php">Buy</a></div>
            <div class="navigation-button"><a href="Sell.php">Sell</a></div>
            <div class="navigation-button"><a href="My Account.php">My Account</a></div>
            <div class="navigation-button"><a href="Feedback.php">Feedback</a></div>
            <div class="navigation-button"><a href="About%20US.php">About Us</a></div>
        </div>
    </div>
</header>
<body>
<span class="featuredCarsSpan">My Account</span>
<label id="yourListingLabel">Your Listings</label><br><br>

<div id="myAccountDiv">
    <div id="profilePictureDiv">
        <img id="profilePicture" src="<?php echo(isset($profileImage) ? $profileImage : ''); ?>"/>
    </div>

    <div id="yourDetails">
        <p> <?php echo "Name: " . (isset($name) ? $name : ''); ?> <br><br>
            <?php echo "UserName: " . (isset($username) ? $username : ''); ?> <br><br>
            <?php echo "Contact Email: " . (isset($email) ? $email : ''); ?> <br><br>
            <?php echo "Location: " . (isset($location) ? $location : ''); ?>
        </p>
    </div>


    <div id="carsCurrentlyBeingSold">
        <form action="DeleteListing.php" method="post">
            <p class="carDescription"><?php echo(isset($Car_name[0]) ? $Car_name[0] : ''); ?>
                <br><?php echo(isset($Car_Price[0]) ? $Car_Price[0] : ''); ?><br>
                <?php echo(isset($Car_Kilometers[0]) ? $Car_Kilometers[0] : ''); ?><br></p>
            <input class="checkBoxForCars" type="checkbox" name="Listing[]"
                   value="<?php echo(isset($Car_id[0]) ? $Car_id[0] : ''); ?>">

            <img class="carsForSale" src="<?php echo(isset($Car_Image[0]) ? $Car_Image[0] : ''); ?>">
            <p class="carDescription"><?php echo(isset($Car_name[1]) ? $Car_name[1] : ''); ?>
                <br><?php echo(isset($Car_Price[1]) ? $Car_Price[1] : ''); ?><br>
                <?php echo(isset($Car_Kilometers[1]) ? $Car_Kilometers[1] : '') ?><br></p><br>

            <img class="carsForSale"
                 src="<?php echo(isset($Car_Image[1]) ? $Car_Image[1] : ''); ?>"><br><br><br><br><br><br><br>
            <input class="checkBoxForCars" id="listing2" type="checkbox" name="Listing[]"
                   value="<?php echo(isset($Car_id[1]) ? $Car_id[1] : ''); ?>"><br><br><br>
            <div id="editListingToggles">
                <input type="submit" name="deleteCarDetails" value="Delete Listing">
            </div>
        </form>
    </div>

</div>


<footer class="tweakedFooter">
    <div>
        <div id="footer-navigation-bar-shop-for-a-car">
            <div id="shopForACar"><span>Shop for a Car</span></div>
            <div class="footer-navigation-button"><a href="Buy.php">Cars For Sale</a></div>
            <div class="footer-navigation-button"><a href="Buy.php">Car Deals</a></div>
            <div class="footer-navigation-button"><a href="Buy.php">New Listings</a></div>
        </div>

        <div id="footer-navigation-my-account">
            <div id="my-account"><span>My Account</span></div>
            <div class="footer-navigation-button"><a href="My Account.php">Your Listings</a></div>
            <div class="footer-navigation-button"><a href="">Watching</a></div>
            <div class="footer-navigation-button"><a href="Sell.php">Sell A Car</a></div>
        </div>

        <div id="footer-navigation-about-us">
            <div id="about-us"><span>About Us</span></div>
            <div class="footer-navigation-button"><a href="http://www.weltec.ac.nz">Company Information</a></div>
            <div class="footer-navigation-button"><a href="http://www.weltec.ac.nz">Corporate Information</a></div>
            <div class="footer-navigation-button"><a href="Contact%20Us.php">Contact Us</a></div>
        </div>

        <div id="footer-navigation-research-cars">
            <div id="research-cars"><span>Research Cars</span></div>
            <div class="footer-navigation-button"><a href="Buy.php">Car Research</a></div>
            <div class="footer-navigation-button"><a href="Buy.php">New Cars</a></div>
            <div class="footer-navigation-button"><a href="Buy.php">Used Cars</a></div>
        </div>
    </div>
</footer>
</body>
</html>