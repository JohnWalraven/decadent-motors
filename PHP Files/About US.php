<?php
/**
 * Created by PhpStorm.
 * User: JWalraven
 * Date: 24/05/2016
 * Time: 11:09 AM
 */
//This is a comment about a comment
session_start();
if (!isset($_SESSION["emailAddress"]) && !isset($_SESSION["password"])) {
    $loginStatus = "Currently not logged in";
    $accessToSell = false;
    $accessToAccount = false;
} else {
    $loginStatus = "Logged in as " . (isset($_SESSION['name']) ? $_SESSION['name'] : '') . "<input type='submit' value='Sign out'/>";
    $accessToSell = true;
    $accessToAccount = true;
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Decadent Motor Trade</title>
    <link rel="stylesheet" type="text/css" href="../CSS/Theme.css">
    <script src="../Javascript/JavaScriptFile.js"></script>
</head>

<header>
    <div id="signOut">
        <form method="post" action="Logout.php">
            <label><?php echo $loginStatus; ?></label>
        </form>
    </div>
    <div id="signIn">
        <form method="post" action="login.php">
            <input type="text" id="loginEmailAddress" name="inputLoginEmailAddress"
                   placeholder="Email Address or Username"
                   onblur="testValidity('loginEmailAddress','requiredLoginEmailAddress' )">
            <input type="password" id="loginPassword" name="inputLoginPassword" placeholder="Password"
                   onblur="testValidity('loginPassword','requiredLoginPassword' )">
            <input type="submit" name="submitDetails" value="Sign In" onclick="validateLogin()"><br>
            <label class="requiredField" id="requiredLoginEmailAddress">This field is required</label>
            <label class="requiredField" id="requiredLoginPassword" style="padding-left:6%; ">This field is
                required</label>
        </form>
    </div>
    <div id="header">
        <div id="logo-div">
            <a href="Home Page.php"><img class="logo-picture" src="../Images/Drawing.png"/></a>
        </div>
        <div id="navigation-bar">
            <div class="navigation-button"><a href="Buy.php">Buy</a></div>
            <div class="navigation-button"><a href="<?php if ($accessToSell == true) {
                    echo 'Sell.php';
                } ?>">Sell</a></div>
            <div class="navigation-button"><a href="<?php if ($accessToSell == true) {
                    echo 'My%20Account.php';
                } ?>">My Account</a></div>
            <div class="navigation-button"><a href="Feedback.php">Feedback</a></div>
            <div class="navigation-button"><a href="About US.php">About Us</a></div>
        </div>
    </div>
</header>
<body>
<div id="about-us-paragraph">
    <p id="pretentious-paragraph">
        <img src="../Images/Drawing.png" style="width: 30%; height: 110px;"><br><br>
        At Decadent Motor Trade we love to buy and sell egregiously expensive cars,<br>
        and help you fulfill your dreams of owning the car you desire.<br>
    </p>
</div>
<div>
    <div id="BadgesTable">
        <div class="table-row">
            <div class="table-cell">
                <img class="Badges" src="../Car%20Badges/ferrari.png"/>
            </div>
            <div class="table-cell">
                <img class="Badges" src="../Car%20Badges/alfa_romeo.png"/>
            </div>
            <div class="table-cell">
                <img class="Badges" src="../Car%20Badges/lamborghini.png"/>
            </div>
            <div class="table-cell">
                <img class="Badges" src="../Car%20Badges/maybach.png"/>
            </div>
            <div class="table-cell">
                <img class="Badges" src="../Car%20Badges/marchedrs.png"/>
            </div>
            <div class="table-cell">
                <img class="Badges" src="../Car%20Badges/pagani.png"/>
            </div>
            <div class="table-cell">
                <img class="Badges" src="../Car%20Badges/aston_martin.png"/>
            </div>
            <div class="table-cell">
                <img class="Badges" src="../Car%20Badges/bugatti.png"/>
            </div>
        </div>
    </div>
</div>
<footer>
    <div>
        <div id="footer-navigation-bar-shop-for-a-car">
            <div id="shopForACar"><span>Shop for a Car</span></div>
            <div class="footer-navigation-button"><a href="Buy.php">Cars For Sale</a></div>
            <div class="footer-navigation-button"><a href="Buy.php">Car Deals</a></div>
            <div class="footer-navigation-button"><a href="Buy.php">New Listings</a></div>
        </div>

        <div id="footer-navigation-my-account">
            <div id="my-account"><span>My Account</span></div>
            <div class="footer-navigation-button"><a href="<?php if ($accessToSell == true) {
                    echo 'My%20Account.php';
                } ?>">Your Listings</a></div>
            <div class="footer-navigation-button"><a href="">Watching</a></div>
            <div class="footer-navigation-button"><a href="Sell.php">Sell A Car</a></div>
        </div>

        <div id="footer-navigation-about-us">
            <div id="about-us"><span>About Us</span></div>
            <div class="footer-navigation-button"><a href="www.weltec.ac.nz">Company Information</a></div>
            <div class="footer-navigation-button"><a href="www.weltec.ac.nz">Corporate Information</a></div>
            <div class="footer-navigation-button"><a href="Contact%20Us.html">Contact Us</a></div>
        </div>

        <div id="footer-navigation-research-cars">
            <div id="research-cars"><span>Research Cars</span></div>
            <div class="footer-navigation-button"><a href="Buy.php">Car Research</a></div>
            <div class="footer-navigation-button"><a href="Buy.php">New Cars</a></div>
            <div class="footer-navigation-button"><a href="Buy.php">Used Cars</a></div>
        </div>
    </div>
</footer>
</body>
</html>